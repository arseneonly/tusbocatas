import firebase, { User } from 'firebase/app';
import 'firebase/auth';        // for authentication
import 'firebase/storage';     // for storage
import 'firebase/database';    // for realtime database

import { firebaseConfig } from './firebase-config';
import { Utils } from '../global/utils';
import { Order, UserOrder } from '../global/entities/orders';
import { Bocata } from '../global/entities/bocata';
import { Entities } from '../global/entities/booking';
import { Volunteer, VolunteerBooking, VolunteerCoords } from '../global/entities/volunteer';
import { payment } from '../global/entities/payment';
import { MessageUser } from '../global/entities/message';

export class FirebaseDB{
    static auth: firebase.auth.Auth;

    static Init() {
        firebase.initializeApp(firebaseConfig);
        this.Auth();
    }

    static Auth(){
        this.auth = firebase.auth();
    }

    static AuthChange(callback){
        firebase.auth().onAuthStateChanged(callback);
    }

    static SignIn(email: string, pwd: string, callThen: any, callCatch: any){
        this.auth.signInWithEmailAndPassword(email, pwd).then(callThen).catch(callCatch);
    }

    static SignOut(callThen: any, callCatch: any){
        firebase.auth().signOut().then(callThen).catch(callCatch);
    }

    static CreateUser(email: string, pwd: string, callThen: any, callCatch: any){
        this.auth.createUserWithEmailAndPassword(email, pwd).then(callThen).catch(callCatch);
    }

    static GetStorageDownloadURL(name: string, callThen: any){
        firebase.storage().ref().child(name).getDownloadURL().then(callThen);
    }

    static GetCurrentUser() : firebase.User {
        return firebase.auth().currentUser;
    }

    static SetCurrentUser(data: any, callThen: any, callCatch: any){
        this.GetCurrentUser().updateProfile(data).then(callThen).catch(callCatch);
    }

    static GetStorage(ref: string) : firebase.storage.Reference{
        return firebase.storage().ref(ref);
    }

    static GetCollection(ref: string, snap: any) {
        firebase.database().ref().child(ref).on('value', snap);
    }

    static SetCollection(ref: string, data: any, callThen: any, callCatch: any) {
        firebase.database().ref(ref).set(data).then(callThen).catch(callCatch);
    }

    static RemoveElementCollection(ref: string, callThen: any, callCatch: any) {
        firebase.database().ref(ref).remove().then(callThen).catch(callCatch);
    }

    static SendEmailResetPassword(email: string, callThen: any, callCatch: any){
        if(email){
            this.auth.sendPasswordResetEmail(email)
                .then(callThen)
                .catch(callCatch);
        }
    }
}

export namespace BR {

    export class Booking{

        static Add(user: User, bocata: Bocata) {
            
            if (user && bocata){
                
                let id = Utils.GenerateId();
                let date = Utils.GenerateDate('-');
                let order = new Order(bocata, new UserOrder(user.displayName, user.photoURL, user.uid));
                let booking = new Entities.Booking(bocata, 0, Utils.GenerateDate(), id,user.uid, new Date(), 0, null);

                var updates = {};
                updates['/orders/' + date + '/' + id ] = order;
                updates['/bookings/' + user.uid + '/' + id] = booking;
                
                return firebase.database().ref().update(updates); 
            }
            
            return null;
        }

        static Remove(uid: string, booking: Entities.Booking) : boolean {
            
            if (uid && booking){
                firebase.database().ref('orders/' + Utils.GenerateDate('-') + '/' + booking.id).remove();
                firebase.database().ref('bookings/' + uid + '/' + booking.id).remove();               
                return true;
            }
            
            return false;  
        }

        static RemoveByList(uid: string, bookingIds: Array<string>){
            if(uid && bookingIds && bookingIds.length > 0){
                var updates = {};
                
                bookingIds.forEach((id) => {  
                    updates['bookings/' + uid+ "/" + id] = null;
                });

                return firebase.database().ref().update(updates);
            }

            return null;
        }

        static SetStatus(volunteer: Volunteer, bookings: Entities.Booking[]){

            if (volunteer && volunteer.uid && bookings && bookings.length > 0){
                var updates = {};

                updates['volunteer/' + Utils.GenerateDate('-') ] = volunteer;
    
                bookings.forEach((book) => {  
                    book.status = 1;
                    updates['bookings/' + book.uid + '/' + book.id] = book;
                });
            }
                
            return firebase.database().ref().update(updates);
        }

        static SetTypePay(booking: Entities.Booking){

            if (booking && booking.uid && booking.id ){
                var updates = {};

                updates['bookings/'+ booking.uid + "/" + booking.id + "/typePay"] = booking.typePay;                
                updates['orders/'+ Utils.ReplaceAll(booking.date,"/","-") + "/" + booking.id + "/typePay"] = booking.typePay;
                
                return firebase.database().ref().update(updates);
            }

            return null;
        }

        static OrderBy(uid: string, snap) {
            return firebase.database().ref('/bookings/'+uid).orderByChild('dateCreation').limitToLast(20).on('value', snap);
        }

        static SetVolunteer(voluntee: Volunteer, orders: Array<Order>){
            if(voluntee && voluntee.uid && orders && orders.length > 0){
                var updates = {};
                
                orders.forEach((order) => {  
                    updates['bookings/' + order.user.uid + "/" + order.bookingId + "/volunteer"] = new VolunteerBooking(
                        voluntee.uid, 
                        voluntee.name, 
                        voluntee.photo
                    );
                });

                return firebase.database().ref().update(updates);
            }

            return null;
        }
    
    }

    export class Volunteers{
        static SetVolunteerCoords(date, volunteerCoord: VolunteerCoords){

            if (date && 
                volunteerCoord && 
                volunteerCoord.latitude &&
                volunteerCoord.longitude && 
                volunteerCoord.lastBeat){
                    
                var updates = {};

                updates['volunteer/'+ date + "/coords"] = volunteerCoord;                
                
                return firebase.database().ref().update(updates);
            }

            return null;
        }
        static RemoveHistoric(minDate: Date, maxDate: Date){

            if(minDate && maxDate){
                var dates = Utils.generateDates(maxDate,minDate);
                var updates = {};

                dates.forEach((date) => {  
                    updates['volunteer/' + date ] = null;
                });
                
                return firebase.database().ref().update(updates);
            }

            return null;
        }
    }

    export class Orders{
        static RemoveHistoric(minDate: Date, maxDate: Date){

            if(minDate && maxDate){
                var dates = Utils.generateDates(maxDate,minDate);
                var updates = {};

                dates.forEach((date) => {  
                    updates['orders/' + date ] = null;
                });
                
                return firebase.database().ref().update(updates);
            }

            return null;
        }
    }

    export class Payments{

        static Set(voluntee: Volunteer, orders: Array<Order>){
            if(voluntee && voluntee.uid && orders && orders.length > 0){
                var updates = {};
                
                orders.forEach((order) => {  
                    if(order.user.uid != voluntee.uid){
                        updates['payments/' + voluntee.uid + '/' + order.user.uid + "/" + order.bookingId] = new payment(
                            (order.typePay > 0 ? 2 : 1),
                            order.bookingId, 
                            Utils.GenerateDate("-"), 
                            order.bocata.precio, 
                            order.user.photo, 
                            order.user.name,
                            order.user.uid,
                            order.typePay
                        );
                    }
                });

                return firebase.database().ref().update(updates);
            }

            return null;
        }

        static SetByPayment(volunteerId:string, payment: payment){
            if(volunteerId && payment){
                var updates = {};

                updates['bookings/' + payment.uid + "/" + payment.bookingId + "/typePay" ] = payment.typePay;
                updates['payments/' + volunteerId + "/" + payment.uid + "/"+ payment.bookingId ] = payment;

                return firebase.database().ref().update(updates);
            }
        }

        static SetByBooking(volunteerId:string, paymenUid: string, bookingId: string, tipePay: number, status: number){
            if(volunteerId && paymenUid && bookingId){
                var updates = {};

                updates['payments/' + volunteerId + "/" + paymenUid + "/"+ bookingId + "/typePay"] = tipePay;
                updates['payments/' + volunteerId + "/" + paymenUid + "/"+ bookingId + "/status"] = status;

                return firebase.database().ref().update(updates);
            }
        }

        static SetStatusByBooking(volunteerId:string, paymenUid: string, bookingId: string, status: number){
            if (volunteerId && paymenUid && bookingId){
                var updates = {};

                updates['payments/' + volunteerId + "/" + paymenUid + "/"+ bookingId + "/status"] = status;

                return firebase.database().ref().update(updates);
            }
        }
    }

    export class BLBocata{

        static UpdatePrice(userId: string, bocataId: string, precio: string) {
            
            if (userId && bocataId && parseInt(precio) > 0){
                var updates = {};
                updates['/mybocata/' + userId + '/' + bocataId + '/precio'] = precio;
                return firebase.database().ref().update(updates); 
            }
            
            return null;
        }

        static Remove(uid: string, bocata: Bocata, callThen: any) : boolean {
            
            if (uid && bocata ){
                firebase.database().ref('mybocata/' + uid + '/' + bocata.id).remove().then(callThen);              
                return true;
            }
            
            return false;
        }
        
    }

    export class Message{

        static AddText(text: Message)
        {
            if (text){
                let date = Utils.GenerateDate('-');
                let id = Utils.GenerateId();

                var updates = {};
                updates['/messages/' + date + '/texts/' + id] = text;
                
                return firebase.database().ref().update(updates); 
            }
            
            return null;
        }

        static GetOrderBy(date: string, snap) {
            return firebase.database().ref('/messages/'+date+'/texts').orderByChild('dateCreation').on('value', snap);
        }

        static AddUser(user: User) {
            
            if (user){
                let date = Utils.GenerateDate('-');
                let message = new MessageUser(user.photoURL,user.displayName, user.uid)

                var updates = {};
                updates['/messages/' + date + '/participants/' + user.uid] = message;
                
                return firebase.database().ref().update(updates); 
            }
            
            return null;
        }

        static RemoveHistoric(minDate: Date, maxDate: Date){

            if(minDate && maxDate){
                var dates = Utils.generateDates(maxDate,minDate);
                var updates = {};

                dates.forEach((date) => {  
                    updates['messages/' + date ] = null;
                });
                
                return firebase.database().ref().update(updates);
            }

            return null;
        }

        static RemoveUser(uid: string) : boolean {
            
            if (uid ){
                firebase.database().ref('messages/' + Utils.GenerateDate('-') + '/participants/' + uid).remove();           
                return true;
            }
            
            return false;  
        }
    }
}