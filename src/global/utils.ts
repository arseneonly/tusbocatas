
export class Utils{

    static GenerateId() : string{
        return Math.random().toString(36).substr(2)
    }

    static GenerateDate(separator: string = '/') : string {
        var d = new Date();
        return [Utils.Pad(d.getDate()), Utils.Pad(d.getMonth()+1), d.getFullYear()].join(separator);
    }

    static GenerateHour(separator: string = ':') : string {
        var d = new Date();
        return [Utils.Pad(d.getHours()), Utils.Pad(d.getMinutes())].join(separator);
    }

    static GenerateHourWithDate(separator: string = ':', date: Date) : string {
        var d = new Date(date);
        return [Utils.Pad(d.getHours()), Utils.Pad(d.getMinutes()), Utils.Pad(d.getSeconds())].join(separator);
    }

    static FormatDate(separator: string = '-', d: Date) : string {
        if(d)
            return [Utils.Pad(d.getDate()), Utils.Pad(d.getMonth()+1), d.getFullYear()].join(separator);

        return null;
    }

    static Pad(s) : string { return (s < 10) ? '0' + s : s; }

    static ReplaceAll(str, find, replace){
        return str.replace(new RegExp(find, 'g'), replace);
    }

    static checkNotification() {
        // Comprobamos si el navegador soporta las notificaciones
        if ("Notification" in window) {
            Notification.requestPermission();
        }
    }

    static sendNotificationLasTime(name){
        if(name){
            var options = {
                body: name+" se presenta voluntario! Reserva tu bocata si aún no lo has hecho!",
                icon: "https://firebasestorage.googleapis.com/v0/b/tusbocatas-3d146.appspot.com/o/logo.png?alt=media&token=0d57e032-8091-4aaf-a8b7-7d0bd2e06dce",
                timeout: 10000000,
                onClick: () => {
                    window.location.href = "https://www.tusbocatas.com"; 
                }
            }
            var n = new Notification("TusBocatas",options);
            setTimeout(n.close.bind(n), 5000); 
        }
    }

    static validateEmail(email: string){
        if(email){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
            return re.test(String(email).toLowerCase());
        }

        return false;
    }

    static generateDates(start, end) {
        for(var arr=[],dt=start; dt<=end; dt.setDate(dt.getDate()+1)){
            arr.push(Utils.FormatDate('-',new Date(dt)));
        }
        return arr;
    };
}
