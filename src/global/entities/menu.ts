export class MenuHeader{
    public id: number = 0;
    public text: string = '';
    public url: string = '';
    public selected: boolean = false;
    public icon: string = '';
    public viewbox: string = '';

    public constructor(id: number = 0, text: string, url: string, selected: boolean, icon: string, viewbox: string ){
        this.text = text;
        this.url = url;
        this.id = id;
        this.selected = selected;
        this.icon = icon;
        this.viewbox = viewbox;
    }
}