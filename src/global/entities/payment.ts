
export class payment{
    public status: StatusPayment = 0;
    public bookingId: string = '';
    public date: string = '';
    public price: number = 0;
    public photo: string = '';
    public name: string = '';
    public uid: string = '';
    public typePay: number = 0;

    public constructor(status, bookingId, date, price, photo, name, uid, typePay){
        this.status = status,
        this.bookingId = bookingId, 
        this.date = date, 
        this.price = price, 
        this.photo = photo
        this.name = name;
        this.uid = uid;
        this.typePay = typePay;
    }
}

enum StatusPayment{
    NotDefined = 0,
    Pendiente = 1,
    Pagada = 2,
    BookingAnulada = 3
}

