
export class Bocata{
    public name: string = '';
    public photo: string = '';
    public precio: number = 0;
    public ingredientes: Array<Ingrediente>;
    public strIngredientes: string = '';
    public colors: Colors;
    public id: string = '';
    public tostado: boolean = false;
}

export class Ingrediente{
    public name: string = '';
    public icon: string = '';
    public viewbox: string = '';
    public width: number = 0;
    public height: number = 0;
    public selected: boolean = false;

    public constructor(name: string, icon: string, viewbox: string, width: number, height: number){
        this.name = name;
        this.icon = icon;
        this.viewbox = viewbox;
        this.width = width;
        this.height = height;
    }
}

export class Colors{
    public color1: string = '';
    public color2: any = '';

    public constructor(color1, color2){
        this.color1 = color1;
        this.color2 = color2; 
    }
}

export class PriceBocata{
    public UserId: string = '';
    public BocataId: string = '';
    public Price: number = 0;
}
