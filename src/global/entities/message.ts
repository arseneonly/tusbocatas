export class Message{
    public photo: string = '';
    public name: string = '';
    public text: string = '';
    public time: string = '';
    public dateCreation: Date;

    public constructor(photo, name, text, time, dateCreation){
        this.photo = photo;
        this.name = name;
        this.text = text;
        this.time = time;
        this.dateCreation = dateCreation;
    }
}

export class MessageUser{
    public photo: string = '';
    public name: string = '';
    public uid: string = '';
    public constructor(photo, name, uid){
        this.photo = photo;
        this.name = name;
        this.uid = uid;
    }
}