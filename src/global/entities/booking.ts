import { Bocata } from "./bocata";
import { VolunteerBooking } from "./volunteer";

export namespace Entities{

    export class Booking{
        public bocata: Bocata = null;
        public date: string = '';
        public dateCreation: Date;
        public status : BookingStatus;
        public selected: boolean = false;
        public id: string = '';
        public uid: string = '';
        public typePay: BookingTypePay = 0;
        public volunteer: VolunteerBooking = null;
        
        public constructor(
            bocata: Bocata, 
            status: BookingStatus = 
            BookingStatus.Pending, 
            date: string, 
            id: string, 
            uid: string, 
            dateCreation: Date, 
            pay: BookingTypePay, 
            volunteer: VolunteerBooking){
                this.date = date;
                this.status = status;
                this.id = id;
                this.bocata = bocata;
                this.uid = uid;
                this.dateCreation = (dateCreation ? new Date(dateCreation) : new Date());
                this.typePay = pay;
                this.volunteer = volunteer;
        }   
    }
    
}

enum BookingStatus {
    Pending = 0,
    Complete = 1
}

enum BookingTypePay {
    NotDefined = 0,
    Bizum = 1,
    Money = 2
}