import { Bocata } from "./bocata";

export class Order{
    public bocata: Bocata = null;
    public date: Date = null;
    public status : BookingStatus;
    public user : UserOrder;
    public bookingId: string = null;
    public typePay: BookingTypePay = 0;

    public constructor(
        bocata: Bocata, 
        user: UserOrder, 
        status: BookingStatus = BookingStatus.Pending, 
        date: Date = new Date(), 
        bookingId: string = null, 
        typePay: BookingTypePay = 0){
            this.bocata = bocata;
            this.date = date;
            this.status = status;
            this.user = user;
            this.bookingId = bookingId;
            this.typePay = typePay;
    }
}

export class UserOrder{
    public name: string;
    public photo: string;
    public uid: string;

    public constructor(name, photo, uid){
        this.name = name;
        this.photo = photo;
        this.uid = uid;
    }
}

export class ResumeOrder{
    public bocata: string;
    public cantidad: number;
    public ingredientes: Array<string>;

    public constructor(bocata, cantidad, ingredientes){
        this.bocata = bocata;
        this.cantidad = cantidad;
        this.ingredientes = ingredientes;
    }
}

enum BookingStatus {
    Default = 0,
    Pending = 1,
    Complete = 2,
    Delete = 3,
}

enum BookingTypePay {
    NotDefined = 0,
    Bizum = 1,
    Money = 2
}