
export class Ingredientes{
    public name: string = '';
    public photo: string = '';
    public selected: boolean = false;

    public constructor(name:string, photo:string){
        this.name = name;
        this.photo = photo;
    }
}