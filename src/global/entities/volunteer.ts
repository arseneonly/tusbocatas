export class Volunteer{
    public name: string = '';
    public photo: string = '';
    public date: string = '';
    public uid: string = '';
    public HourStart: string;
    public HourEnd: string;
    public finished: boolean = false;
    public coords: VolunteerCoords;

    public constructor(name:string, photo:string, date:string, uid: string, HourStart: string, HourEnd: string, finished: boolean){
        this.name = name;
        this.photo = photo;
        this.date = date;
        this.uid = uid;
        this.HourStart = HourStart;
        this.HourEnd = HourEnd;
        this.finished = finished;
        this.coords = new VolunteerCoords();
    }
}

export class VolunteerBooking{
    public name: string = '';
    public uid: string = '';
    public photo: string = '';

    public constructor(uid, name, photo){
        this.uid = uid;
        this.name = name;
        this.photo = photo;
    }
}

export class VolunteerCoords {
    public allowGPS: boolean = false;
    public latitude: string = '';
    public longitude: string = '';
    public lastBeat: Date = null;
}