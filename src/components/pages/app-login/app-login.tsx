import { Component, State, Prop, EventEmitter, Event } from "@stencil/core";
import { RouterHistory } from "@stencil/router";
import { FirebaseDB } from "../../../helpers/firebaseDB";
import { Utils } from '../../../global/utils';

@Component({
  tag: "app-login",
  styleUrl: "app-login.scss",
  shadow: true
})
export class App {
  @Prop() history: RouterHistory;

  @State() auth: firebase.auth.Auth;
  @State() showLogin: boolean = true;
  @State() showRegister: boolean = false;
  @State() showResetPwd: boolean = false;
  @State() isLogged: boolean = false;
  @State() anyError: string = "";
  @State() canRender: boolean = false;
  @State() disabledButton: boolean = false;
  @State() infoSuccess: string = '';

  @State() email: string = "";
  @State() pwd: string = "";
  @State() pwdConfirmation: string = "";
  @State() name: string = "";

  @Event() eventLoad: EventEmitter;

  //#region Circle Life

  componentWillLoad() {
    FirebaseDB.AuthChange((user: firebase.User) => {
      if (user) {
        if (!user.displayName && !user.photoURL && this.name) {
          FirebaseDB.GetStorageDownloadURL(
            "avatar_default.png",
            (url: string) => {
              FirebaseDB.SetCurrentUser(
                { photoURL: url, displayName: this.name },
                () => {
                  this.history.push("/menu", {});
                },
                () => { }
              );
            }
          );
        } else this.history.push("/menu");
      } else {
        this.canRender = true;
        this.eventLoad.emit(false);
      }
    });
  }

  //#endregion

  //#region Set
  setLogin() {
    this.disabledButton = true;

    if (this.validateLogin()) {
      FirebaseDB.SignIn(
        this.email,
        this.pwd,
        () => {
          this.disabledButton = false;
          this.history.push("/menu", {});
        },
        () => {
          this.disabledButton = false;
          this.anyError = "Email o contraseña incorrecta";
        }
      );
    } else {
      this.disabledButton = false;
      this.anyError = "Todos los campos obligatorios";
    }
  }

  setRegister() {
    this.anyError = "";
    this.disabledButton = true;

    if (this.validateLogin(false)) {
      FirebaseDB.CreateUser(
        this.email,
        this.pwd,
        () => {
          this.canRender = false;
        },
        () => {
          this.anyError =
            "Error al crear la cuenta. Si persiste, soporte@tusbocatas.com";
        }
      );
    }

    this.disabledButton = false;
  }

  setResetPwd() {

    this.disabledButton = true;
    this.anyError = '';
    this.infoSuccess = '';
    if (this.email && Utils.validateEmail(this.email)) {

      FirebaseDB.SendEmailResetPassword(this.email,
        () => { this.infoSuccess = "Revisa tu buzón de correo para reestablecer tu contraseña"; },
        () => { this.anyError = "No se ha podido enviar el email"; }
      )

    }
    else {
      this.anyError = "El email no es válido";
    }

    this.disabledButton = false;
  }
  //#endregion

  //#region Validate
  validateLogin(isLogin: boolean = true) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (isLogin) {
      if (!this.email || !this.pwd) {
        this.anyError = "Campos obligatorios";
        return false;
      } else if (!re.test(String(this.email).toLowerCase())) {
        this.anyError = "Email inválido";
        return false;
      } else if (this.pwd.length < 6) {
        this.anyError = "Contraseña inválido, mínimo 6 caracteres";
        return false;
      }
    } else {
      if (!this.email || !this.pwd || !this.name || !this.pwdConfirmation) {
        this.anyError = "Campos obligatorios";
        return false;
      } else if (!re.test(String(this.email).toLowerCase())) {
        this.anyError = "Email inválido";
        return false;
      } else if (this.name.length < 5) {
        this.anyError = "El nombre tiene que tener 5 caracteres";
        return false;
      } else if (this.pwd.length < 6) {
        this.anyError = "Contraseña inválido, mínimo 6 caracteres";
        return false;
      } else if (this.pwd != this.pwdConfirmation) {
        this.anyError = "Las contraseñas no coinciden";
        return false;
      } else if (!this.email.includes("@tusmedia.com")) {
        this.anyError = "El mail solo puede ser _@tusmedia.com";
        return false;
      }
    }

    return true;
  }
  //#endregion

  //#region Handle

  handlerLogin() {
    this.showLogin = true;
    this.showRegister = false;
    this.showResetPwd = false;
    this.email = "";
    this.pwd = "";
    this.anyError = "";

  }

  handlerRegistry() {
    this.showRegister = true;
    this.showLogin = false;
    this.showResetPwd = false;
    this.email = "";
    this.pwd = "";
    this.pwdConfirmation = "";
    this.name = "";
    this.anyError = "";
  }

  handlerReset() {
    this.showLogin = true;
    this.showRegister = false;
    this.showResetPwd = false;
  }

  handlerClose() {
    this.showLogin = false;
    this.showRegister = false;
    this.email = "";
    this.pwd = "";
    this.anyError = "";
    this.pwdConfirmation = "";
    this.name = "";
  }

  handlerEmail(event: any) {
    this.email = event.target.value;
  }

  handlerPwd(event: any) {
    this.pwd = event.target.value;
  }

  handlerName(event: any) {
    this.name = event.target.value;
  }

  handlerPwdConfirmation(event: any) {
    this.pwdConfirmation = event.target.value;
  }

  handleEnterLogin(e: KeyboardEvent) {
    if (e && e.keyCode == 13) this.setLogin();
  }

  handleEnterCreate(e: KeyboardEvent) {
    if (e && e.keyCode == 13) this.setRegister();
  }

  handlerResetPassword() {
    this.email = "";
    this.showResetPwd = true;
    this.showLogin = false;
    this.showRegister = false;
  }

  //#endregion

  render() {

    return [
      <stencil-route-title pageTitle="Login" />,
      <div class="app-login">
        {this.canRender
          ?
          <div class="content">
            <div class='content-logo'>
              <app-svg class="logo" name="splash" width={50} height={50} color="#fff" viewBox="0 0 155 123" />
              <div class="text">tusbocatas</div>
            </div>
            <div class='cb'></div>

            {this.showLogin ?
              <div class={"login" + (this.showLogin ? " active" : "")}
                onKeyPress={e => this.handleEnterLogin(e)}
              >



                <div class="login-title">Inicio sesión</div>
                <input
                  class="l-e"
                  value={this.email}
                  type="email"
                  name="email"
                  autocomplete="on"
                  placeholder="Email"
                  onInput={event => this.handlerEmail(event)}
                />

                <input
                  class="l-c"
                  value={this.pwd}
                  type="password"
                  name="password"
                  autocomplete="on"
                  placeholder="Contraseña"
                  onInput={event => this.handlerPwd(event)}
                />
                <button
                  class="l-s"
                  onClick={() => this.setLogin()}
                  disabled={this.disabledButton}
                >
                  Acceder
                        </button>
                {this.anyError ? (
                  <p class="l-err">{this.anyError}</p>
                ) : null}

                <div class='content-link'>
                  <span class="link" onClick={() => this.handlerRegistry()}>
                    Regístrate
                  </span>
                  <span class="link" onClick={() => this.handlerResetPassword()}>
                    He olvidado la contraseña
                  </span>
                </div>
              </div>
              : null}

            {this.showRegister ?
              <div
                class={"register" + (this.showRegister ? " active" : "")}
                onKeyPress={e => this.handleEnterCreate(e)}
              >

                <div class="login-title">Regístrate</div>

                <input
                  class="l-n"
                  value={this.email}
                  type="text"
                  placeholder="Email"
                  onInput={event => this.handlerEmail(event)}
                />
                <input
                  class="l-n"
                  value={this.name}
                  type="text"
                  placeholder="Nombre"
                  onInput={event => this.handlerName(event)}
                />
                <input
                  class="l-c"
                  value={this.pwd}
                  type="password"
                  placeholder="Contraseña"
                  onInput={event => this.handlerPwd(event)}
                />
                <input
                  class="l-c"
                  value={this.pwdConfirmation}
                  type="password"
                  placeholder="Repite la Contraseña"
                  onInput={event => this.handlerPwdConfirmation(event)}
                />
                <button
                  class="a-r"
                  onClick={() => this.setRegister()}
                  disabled={this.disabledButton}
                >
                  Regístrate
                    </button>
                {this.anyError ? (
                  <p class="l-err">{this.anyError}</p>
                ) : null}

                <div class='content-link'>
                  <span class="link" onClick={() => this.handlerLogin()}>
                    Volver al login
                    </span>
                </div>
              </div>
              : null}

            {this.showResetPwd ?
              <div class={"reset-pwd" + (this.showResetPwd ? " active" : "")}>
                <span onClick={() => this.handlerReset()}>
                  <app-svg name="arrow-left" width={19} height={19} viewBox="0 0 492 492" />
                </span>
                <div class="login-title">Solicitud de contraseña</div>
                <p>Por favor, inserta tu correo para que podamos enviarte un mail para cambiar la contraseña</p>
                <input class="l-n" value={this.email} type="text" placeholder="Email" onInput={event => this.handlerEmail(event)} />
                <button class="a-r" onClick={() => this.setResetPwd()} disabled={this.disabledButton}>Solicitar</button>
                {this.anyError ? (<p class="l-err">{this.anyError}</p>) : null}
                {this.infoSuccess ? <p class="l-is">{this.infoSuccess}</p> : null}
              </div>
              : null}
          </div>

          : null}
      </div>
    ];
  }
}
