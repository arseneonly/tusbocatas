import { Component, Prop, State, Event, EventEmitter } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { FirebaseDB } from '../../../helpers/firebaseDB';

@Component({
  tag: 'app-profile',
  styleUrl: 'app-profile.scss',
  shadow: true
})
export class AppProfile {

  @Prop() history: RouterHistory;

  @State() canRender: boolean = false;
  @State() photo: string = '';
  @State() name: string = '';
  @State() email: string = '';
  @State() uploadPercent: number = 0;
  @State() refreshHeader: boolean = false;

  @Event() eventRefresh: EventEmitter;
  @Event() eventLoad: EventEmitter;

  //#region Circle Life
    componentWillLoad() {
      FirebaseDB.AuthChange((user) => {
        if(!user)
          this.history.push('/', {});
        else{
          this.canRender = true;
          this.name = user.displayName;
          this.email = user.email;
          this.photo = user.photoURL;

          this.eventLoad.emit(false);
        }
      });
    }
  //#endregion

  //#region Handle

    handleUploadPhoto(e: any){

      let File = e.target.files[0];
      let User = FirebaseDB.GetCurrentUser();
      let StorageRef = FirebaseDB.GetStorage('users/'+ User.uid);
      let Task = StorageRef.put(File);

      Task.on('state_changed', 
      (sna) => { this.uploadPercent = (sna.bytesTransferred / sna.totalBytes) * 100;}, 
      (err) => { console.log(err);}, 
      () => {
        FirebaseDB.GetStorageDownloadURL('users/'+User.uid, (url: string) => {
          this.photo = url;
          FirebaseDB.SetCurrentUser({ photoURL: url},
          () => { this.eventRefresh.emit(url) },
          () => {})
        });
      })
    }

    handleSave(){
      FirebaseDB.SetCurrentUser({ displayName: this.name}, 
        () => { alert('Guardado :)')}, 
        () => {}
      );
    }

    handleName(e: any){
      this.name = e.target.value;
    }

   //#endregion

  render() {

      return ( [<stencil-route-title pageTitle="Perfil"></stencil-route-title>,
        <div class="app-profile">
        {this.canRender ? 
            <div class='a-p-d'>
              <div class='a-p-d-f'>
                <label htmlFor="file-input">
                  <img class='a-p-d-f-f' src={this.photo}></img>
                  <span class='a-p-d-f-s'>Editar</span>
                </label>
                <input id="file-input" class='a-p-d-f-u' type="file" value="Subir" onChange={(e) => this.handleUploadPhoto(e)}></input>
              </div>
              <div class='a-p-d-f'>
                <label class='a-p-d-f-l'>Nombre</label>
                <input class='a-p-d-f-ii' type="text" value={this.name} onInput={(e)=> this.handleName(e)}></input>
              </div>
              <div class='a-p-d-f'>
                <label class='a-p-d-f-l'>Email</label>
                <input class='a-p-d-f-i' type="text" value={this.email} disabled></input>
              </div>
              <button class='a-p-d-g' onClick={() => this.handleSave()}>Guardar</button>
            </div>
        : null}
        </div>
      ]);
    
  }
}
