import { Component, State, Prop, Event, EventEmitter } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { FirebaseDB, BR } from '../../../helpers/firebaseDB';
import { User } from 'firebase';
import { PriceBocata } from '../../../global/entities/bocata';
import { Utils } from '../../../global/utils';

@Component({
  tag: 'app-admin',
  styleUrl: 'app-admin.scss',
  shadow: true
})
export class AppAdmin {

  @Prop() history: RouterHistory;

  @State() result: any;
  @State() canRender: boolean;
  @State() dateNow: string = new Date().toLocaleDateString();
  @State() user: User;
  @State() dateMin: Date;
  @State() days: number;

  @State() oldPrice1: number;
  @State() newPrice1: number;
  @State() oldPrice2: number;
  @State() newPrice2: number;
  @State() listBocata: Array<PriceBocata> = [];

  @Event() eventLoad: EventEmitter;

  //#region Circle Life
  componentWillLoad() {
    FirebaseDB.AuthChange((user) => {

      if (user && user.uid == "wD9DfMdSSKPXvJaxL63dhLkIxqE2") {
        this.user = user;
        this.canRender = true;
        this.eventLoad.emit(false);
      }
      else
        this.history.push('/', {});
    })
  }
  //#endregion


  //#region Handler

  handleDate(event: any) {
    this.dateMin = event.target.value;
  }

  handleDays(event: any) {
    this.days = event.target.value;
  }

  HandlerRemoveVolunteer() {
    if (this.dateMin) {
      var dateMax = new Date(this.dateMin);
      dateMax.setDate(dateMax.getDate() - this.days);

      var dateMin = new Date(this.dateMin);

      BR.Volunteers.RemoveHistoric(dateMin, dateMax).then(() => {
        console.log('remove volunteer');
      })
    }
  }

  HandlerRemoveOrders() {
    if (this.dateMin) {
      var dateMax = new Date(this.dateMin);
      dateMax.setDate(dateMax.getDate() - this.days);

      var dateMin = new Date(this.dateMin);

      BR.Orders.RemoveHistoric(dateMin, dateMax).then(() => {
        console.log('remove orders');
      })
    }
  }

  HandlerRemoveMessage() {
    if (this.dateMin) {
      var dateMax = new Date(this.dateMin);
      dateMax.setDate(dateMax.getDate() - this.days);

      var dateMin = new Date(this.dateMin);

      BR.Message.RemoveHistoric(dateMin, dateMax).then(() => {
        console.log('remove messages');
      })
    }
  }

  HandlerRemoveVolunteerToday() {
    FirebaseDB.RemoveElementCollection('/volunteer/'+Utils.GenerateDate("-"), 
      () => { alert('Borrado!') },
      () => { console.log('mal');}
    );
  }

  HandlerUpdatePrice(type, event) {
    var price = event.target.value;
    if (type == 1)
      this.oldPrice1 = price;
    else if (type == 2)
      this.newPrice1 = price;
    else if (type == 3)
      this.oldPrice2 = price;
    else if (type == 4)
      this.newPrice2 = price;
  }
  //#endregion

  //#region Get
  getPriceBocatas() {
    if (this.oldPrice1 > 0 && this.oldPrice2 > 0 && this.newPrice1 > 0 && this.newPrice2 > 0) {
      this.listBocata =[];
      FirebaseDB.GetCollection('mybocata', (snap: any) => {
        snap.forEach((user) => {
          user.forEach((bocata) => {
            
            var price = 0;

            if (bocata.val().precio == this.oldPrice1)
              price = this.newPrice1;
            else if (bocata.val().precio == this.oldPrice2)
              price = this.newPrice2;

            if (price > 0){
              var priceBocata = new PriceBocata();
              priceBocata.UserId = user.key;
              priceBocata.BocataId = bocata.key;
              priceBocata.Price = price;
              this.listBocata.push(priceBocata); 
              this.listBocata = [...this.listBocata];
              console.log(priceBocata);
            }

          })
        });
      });
    }
  }
  //#endregion

  //#region Get
  setRemove() {
    this.HandlerRemoveVolunteer();
    this.HandlerRemoveOrders();
    this.HandlerRemoveMessage();
  }

  setPriceBocata(){
    if(this.listBocata && this.listBocata.length > 0){
      this.listBocata.forEach((bocata)=>{
        BR.BLBocata.UpdatePrice(bocata.UserId, bocata.BocataId, bocata.Price.toString()).then(() => {
          console.log('updated!');
        });
      })
    }
  }
  //#endregion

  render() {
    return ([
      <stencil-route-title pageTitle="Admin"></stencil-route-title>,
      <div class='app-admin'>
        {this.canRender ?
          <div class='a-ad'>
            <h1>Mantenimiento</h1>

            <h3>Borrar Voluntarios/Orders/Messages</h3>
            <span class='ib'>
              <label>Número de días para atras</label>
              <input type="number" onInput={(e) => this.handleDays(e)} ></input>
            </span>
            <span class='ib'>
              <label>Fecha inicio</label>
              <input type="date" onInput={(e) => this.handleDate(e)} ></input>
            </span>
            <span class='ib'>
              <button onClick={() => this.setRemove()}>Ejecutar</button>
            </span>

            <h3>Actualizar precio bocatas</h3>
            <span class='ib'>
              <label>Antiguo 1 ingrediente</label>
              <input type="number" onInput={(e) => this.HandlerUpdatePrice(1, e)} ></input>
            </span>
            <span class='ib'>
              <label>Nuevo 1 ingrediente</label>
              <input type="number" onInput={(e) => this.HandlerUpdatePrice(2, e)} ></input>
            </span>
            <span class='ib'>
              <label>Antiguo 2 ingrediente</label>
              <input type="number" onInput={(e) => this.HandlerUpdatePrice(3, e)} ></input>
            </span>
            <span class='ib'>
              <label>Nuevo 2 ingrediente</label>
              <input type="number" onInput={(e) => this.HandlerUpdatePrice(4, e)} ></input>
            </span>
            <span class='ib'>
              <button onClick={() => this.getPriceBocatas()}>Obtener</button>
              <span>{this.listBocata.length}</span>
            </span>
            <span class='ib'>
              <button onClick={() => this.setPriceBocata()}>Ejecutar</button>
            </span>

            <h3>Borrar voluntario HOY</h3>
            <button onClick={() => this.HandlerRemoveVolunteerToday()}>Ejecutar</button>
          </div>
          : null}
      </div>
    ]);
  }
}
