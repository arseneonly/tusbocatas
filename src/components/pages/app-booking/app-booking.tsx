import { Component, State, Prop, Event, EventEmitter } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { FirebaseDB, BR } from '../../../helpers/firebaseDB';
import { Entities } from '../../../global/entities/booking';
import { Utils } from '../../../global/utils';

@Component({
  tag: 'app-booking',
  styleUrl: 'app-booking.scss',
  shadow: true
})
export class AppBooking {

  @Prop() history: RouterHistory;

  @State() listBookings: Array<Entities.Booking>;
  @State() listBookingsHoy: Array<Entities.Booking>;
  @State() user: any;
  @State() allBooking: boolean = false;

  @Event() eventRefreshPage: EventEmitter;
  @Event() eventLoad: EventEmitter;

  //#region Circle Life
  componentWillLoad() {
    this.eventRefreshPage.emit(2);
    FirebaseDB.AuthChange((user) => {

      if (user) {
        this.user = user;
        this.getBookings();
        Utils.checkNotification();
      }
      else
        this.history.push('/', {});

    })
  }

  componentDidLoad() {
    if (this.listBookings && this.listBookings.length > 0) {

      var dateMin = new Date();
      dateMin.setDate(dateMin.getDate() - 30);

      let listTempBooking = [];
      this.listBookings.forEach(element => {
        if (new Date(element.date.split("/").reverse().join("-")) < dateMin)
          listTempBooking.push(element.id);
      });

      if (listTempBooking && listTempBooking.length > 0)
        BR.Booking.RemoveByList(this.user.uid, listTempBooking)
    }

  }
  //#endregion


  //#region Get
  getBookings() {


    BR.Booking.OrderBy(this.user.uid, (snap: any) => {
      this.listBookings = [];
      this.listBookingsHoy = [];
      snap.forEach((request) => {
        if (request.val().date == Utils.GenerateDate()) {
          this.listBookingsHoy.push(new Entities.Booking(
            request.val().bocata,
            request.val().status,
            request.val().date,
            request.val().id,
            this.user.uid,
            request.val().dateCreation,
            request.val().typePay,
            request.val().volunteer
          ));
        }
        else {
          this.listBookings.push(new Entities.Booking(
            request.val().bocata,
            request.val().status,
            request.val().date,
            request.val().id,
            this.user.uid,
            request.val().dateCreation,
            request.val().typePay,
            request.val().volunteer
          ));
        }
      });

      this.listBookingsHoy = [...this.listBookingsHoy];
      this.listBookingsHoy.reverse();

      this.listBookings = [...this.listBookings];
      this.listBookings.reverse();

      this.eventLoad.emit(false);
    })
  }
  //#endregion

  //#region Handle
  handleRemove(booking: Entities.Booking) {
    if (booking) {

      if (this.listBookingsHoy.length == 1)
        BR.Message.RemoveUser(this.user.uid);

      if (BR.Booking.Remove(this.user.uid, booking))
        this.getBookings();

    }
  }

  handleCheckPay(booking: Entities.Booking, e: any) {
    if (booking) {
      booking.typePay = e.target.value;

      BR.Booking.SetTypePay(booking);

      if (booking.volunteer && booking.volunteer.uid && booking.typePay > 0)
        BR.Payments.SetByBooking(booking.volunteer.uid, booking.uid, booking.id, booking.typePay, 2);
    }
  }
  //#endregion

  render() {
    return ([
      <stencil-route-title pageTitle="Reservas"></stencil-route-title>,
      <div class='app-booking'>
        <div class='a-b'>
          {(!this.listBookings || this.listBookings.length == 0) && (!this.listBookingsHoy || this.listBookingsHoy.length == 0) ?
            <div class='a-b-a'>Aún no dispones de reservas</div>
            : (this.listBookings && this.listBookings.length > 0) || (this.listBookingsHoy && this.listBookingsHoy.length > 0) ?
              <div class='a-b-b'>
                <section>
                  <div class='a-b-b-t'>Reservas pendientes</div>
                  <p class='app-booking-p'>Tus reservas de hoy</p>
                  {this.listBookingsHoy && this.listBookingsHoy.length > 0 ?
                    <ul class='a-b-b-u'>
                      {this.listBookingsHoy.map((booking) =>
                        <li class={'a-b-b-u-l' + (booking.status == 0 ? '' : ' delivery')}  >
                          <div>
                            <span class='a-b-b-u-l-d'>{booking.date}</span>
                          </div>
                          <div class='a-b-b-u-l-p'>{booking.bocata.precio} €</div>
                          <div class='a-b-b-u-l-str'>
                            <div>
                              <b>{booking.bocata.name}</b>
                              {booking.bocata.tostado ? <span class="a-b-b-u-l-str-t">T</span> : null}
                            </div>
                            <div>{booking.bocata && booking.bocata.strIngredientes ? booking.bocata.strIngredientes : null}</div>
                          </div>
                          <div class='a-b-b-u-l-pay'>
                            <div class='a-b-b-u-l-pay-d'>
                              <select class='app-booking-typepay' onChange={(e) => this.handleCheckPay(booking, e)} >
                                <option value='0' selected={booking.typePay == 0}>Forma pago</option>
                                <option value='1' selected={booking.typePay == 1}>Bizum</option>
                                <option value='2' selected={booking.typePay == 2}>Efectivo</option>
                              </select>
                            </div>
                          </div>
                          <div class='a-b-b-u-l-r'>
                            {booking.status == 0 ?
                              <div onClick={() => this.handleRemove(booking)}>Borrar</div>
                              : null}
                          </div>
                        </li>
                      )}
                    </ul>
                    : <p class='a-b-b-nb'>No dispones de reservas para hoy</p>}
                </section>
                <section>
                  <div class='a-b-b-t'>Historial</div>
                  <p class='app-booking-p'>Todos los pedidos del último mes</p>
                  {this.listBookings && this.listBookings.length > 0 ?
                    <ul class='a-b-b-u'>
                      {this.listBookings.map((booking) =>
                        <li class={'a-b-b-u-l hist ' + (booking.status == 0 ? '' : ' delivery')}>
                          <span class='a-b-b-u-l-d'>{booking.date}</span>
                          <div class='a-b-b-u-l-p'>{booking.bocata.precio} €</div>
                          <span class='a-b-b-u-l-str'>
                            <div><b>{booking.bocata.name}</b></div>
                            <div>{booking.bocata && booking.bocata.strIngredientes ? booking.bocata.strIngredientes : null}</div>
                          </span>
                          <div class='a-b-b-u-l-pay'>
                            <div class='a-b-b-u-l-pay-d'>
                              <select class='app-booking-typepay' onChange={(e) => this.handleCheckPay(booking, e)} >
                                <option value='0' selected={booking.typePay == 0}>Forma pago</option>
                                <option value='1' selected={booking.typePay == 1}>Bizum</option>
                                <option value='2' selected={booking.typePay == 2}>Efectivo</option>
                              </select>
                            </div>
                          </div>
                        
                        </li>
                      )}
                    </ul>
                    : <p class='a-b-b-nb'>Aún no dispones de historial</p>}
                </section>
              </div>
              : null}
        </div>
      </div>
    ]);
  }
}
