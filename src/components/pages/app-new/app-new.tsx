import { Component, State, Prop, Event, EventEmitter } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { Ingrediente, Bocata, Colors } from '../../../global/entities/bocata';
import { FirebaseDB } from '../../../helpers/firebaseDB';

@Component({
  tag: 'app-new',
  styleUrl: 'app-new.scss',
  shadow: true
})
export class AppHome {

  @Prop() history: RouterHistory;

  @State() canRender: boolean;
  @State() listIngredientes: Array<Ingrediente> = [];
  @State() bocata: Bocata = new Bocata();
  @State() name: string = '';
  @State() cost: number = 0;
  @State() anyError: boolean = false;
  @State() color: Colors = null;
  @State() colorSelected: number = 1;

  @Event() handleCloseModal: EventEmitter;

  @Event() eventLoad: EventEmitter;


  //#region Circle Life

  componentWillLoad() {
    this.eventLoad.emit(false);
    this.getIngredients();
    this.handlerColor(1);
  }

  //#endregion

  //#region Get

  getIngredients() {
    FirebaseDB.GetCollection('ingredientes', (snap: any) => {
      snap.forEach((request) => {
        this.listIngredientes.push(new Ingrediente(
          request.val().name,
          request.val().icon,
          request.val().viewbox,
          request.val().width,
          request.val().height
        ));
        this.listIngredientes = [...this.listIngredientes];
        this.canRender = true;
      });
    });
  }

  //#endregion

  //#region Set

  setBocata() {
    if (this.validateBocata()) {
      FirebaseDB.SetCollection('mybocata/' + FirebaseDB.GetCurrentUser().uid + "/" + Math.random().toString(36).substr(2), this.bocata,
        () => {     this.history.push('/menu', {}); },
        () => { }
      );
    }
  }

  //#endregion

  //#region Handle

  handleIngredient(ingrediente: Ingrediente) {
    if (ingrediente && ingrediente.name) {

      this.listIngredientes.find(x => x.name == ingrediente.name).selected = !this.listIngredientes.find(x => x.name == ingrediente.name).selected;
      this.listIngredientes = [...this.listIngredientes];

      if (!this.bocata.ingredientes)
        this.bocata.ingredientes = [];

      var index = this.bocata.ingredientes.indexOf(ingrediente);
      if (index !== -1)
        this.bocata.ingredientes.splice(index, 1);
      else
        this.bocata.ingredientes.push(ingrediente);

      let list = this.bocata.ingredientes.map(item => item.name);
      this.bocata.strIngredientes = list.join(', ').replace(/,\s*$/, "");

      this.cost = 0;
      this.bocata.precio = 0;

      if (this.bocata.ingredientes.length == 1) {
        this.cost = 1.60;
        this.bocata.precio = 1.60;
      }
      else if (this.bocata.ingredientes.length > 1) {
        this.cost = 1.65;
        this.bocata.precio = 1.65;
      }
    }
  }

  handleName(event: any) {
    this.bocata.name = event.target.value;
  }

  handleTostado() {
    this.bocata.tostado = !this.bocata.tostado;
  }

  handleClose() {
    this.canRender = false;
    this.handleCloseModal.emit(false);
  }

  handlerColor(num: number) {
    this.colorSelected = num;

    switch (num) {
      case 1: this.color = new Colors('rgba(20,174,218,0.65)', '#718095'); break;
      case 2: this.color = new Colors('rgba(255,150,70,0.65)', '#9C6200'); break;
      case 3: this.color = new Colors('rgba(5,108,132,0.65)', '#3C5F51'); break;
      case 4: this.color = new Colors('rgba(206,130,86,0.65)', '#9C6200'); break;
      default: this.color = new Colors('rgba(20,174,218,0.65)', '#718095'); break;
    }

    if (this.color && this.color.color1 && this.color.color2)
      this.bocata.colors = this.color;
  }

  //#endregion

  //#region Validate
  validateBocata(): boolean {
    this.anyError = false;
    if (!this.bocata || 
      this.bocata.precio <= 0 || 
      this.bocata.ingredientes.length == 0 || 
      !this.bocata.name) {
      this.anyError = true;
      return false;
    }

    return true;
  }
  //#endregion

  render() {
    return ([
      <div class='app-new'>
        {this.canRender ? [
          <div class='a-n'>
            <div class='a-n-c'>
              <div class='app-new-bocata'>Nuevo bocadillo</div>
              <div>
                <label class="a-n-c-cb">Nombre</label>
                <input class="a-n-c-bu" onInput={(e) => this.handleName(e)} type="text"></input>
                <label class="a-n-c-cb">Ingredientes</label>
                <div class='a-n-c-i'>
                  {this.listIngredientes.map((ing) =>
                    <div class={"a-n-c-i-i" + (ing.selected ? " selected" : " ")} onClick={() => this.handleIngredient(ing)}>
                      <div class='a-n-c-i-i-n'>{ing.name}</div>
                      <div class='a-n-c-i-i-s'>
                        <app-svg name="check" width={15} height={15} viewBox="0 0 24 24" color="#000"></app-svg>
                      </div>
                    </div>
                  )}
                </div>
                <label>Precio</label>
                <div class='a-n-c-p'>{this.cost} €</div>
                <div class='a-n-c-i'>
                  <label htmlFor='tostado'>Tostado </label>
                  <input type="checkbox" id="tostado" onClick={() => this.handleTostado()}></input>
                </div>
                {this.anyError ? <div class='a-n-c-err'>Todos los campos son obligatorios</div> : null}
              </div>
              <button class='a-n-c-b' onClick={() => this.setBocata()}>Guardar</button>
            </div>
          </div>] : null}
      </div>
    ]);
  }
}