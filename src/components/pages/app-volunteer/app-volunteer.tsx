import { Component, Prop, State, EventEmitter, Event } from '@stencil/core';
import { FirebaseDB, BR } from '../../../helpers/firebaseDB';
import { RouterHistory } from '@stencil/router';
import { Utils } from '../../../global/utils';
import { Volunteer, VolunteerCoords } from '../../../global/entities/volunteer';
import { Order, ResumeOrder } from '../../../global/entities/orders'
import { Entities } from '../../../global/entities/booking';
import { payment } from '../../../global/entities/payment';


@Component({
  tag: 'app-volunteer',
  styleUrl: 'app-volunteer.scss',
  shadow: true
})
export class AppVolunteer {

  @Prop() history: RouterHistory;

  @State() user: any;
  @State() volunteer: Volunteer = null;
  @State() listOrders: Array<Order> = [];
  @State() listBookings: Array<Entities.Booking> = [];
  @State() listPayments: Array<payment> = [];
  @State() totalPay: string;
  @State() totalBocata: number = 0;
  @Event() eventLoad: EventEmitter;
  @State() listResumeCaliente: Array<ResumeOrder> = [];
  @State() listResumeFrio: Array<ResumeOrder> = [];
  @State() intervalAllowGPS: any = null;

  //#region Circle Life
  componentWillLoad() {
    FirebaseDB.AuthChange((user) => {
      if (user) {
        this.user = user;
        this.getOrders();
        this.getPayments();
      }
      else
        this.history.push('/', {});
    })
  }

  componentWillUpdate() {
  }
  //#endregion

  //#region Get
  getVolunteer() {
    FirebaseDB.GetCollection('volunteer/' + Utils.GenerateDate('-'), (snap: any) => {
      if (snap && snap.val()) {
        this.volunteer = new Volunteer(
          snap.val().name,
          snap.val().photo,
          snap.val().date,
          snap.val().uid,
          snap.val().HourStart,
          snap.val().HourEnd,
          snap.val().finished
        );
      }
    });
  }

  getOrders() {
    FirebaseDB.GetCollection('orders/' + Utils.GenerateDate('-'), (snap: any) => {

      //Reset
      this.listOrders = [];
      this.listResumeCaliente = [];
      this.listResumeFrio = [];

      this.totalBocata = 0;
      this.totalPay = null;

      //Get
      snap.forEach((request) => {
        this.listOrders.push(new Order(
          request.val().bocata,
          request.val().user,
          request.val().status,
          request.val().date,
          request.key,
          request.val().typePay)
        );
      })

      this.listOrders = [...this.listOrders];

      let totalPayOrders = 0;


      this.listOrders.forEach((order) => {

        let newBocataIngredientes = Array
          .from(order.bocata.ingredientes, p => p.name)
          .sort((a, b) => a.localeCompare(b));

        if (order.bocata.tostado) {
          var index = this.listResumeCaliente.findIndex(x => x.ingredientes.sort().every(function (value, index) {
            return value === newBocataIngredientes.sort()[index]
          }));

          if (index !== -1)
            this.listResumeCaliente[index].cantidad++;
          else
            this.listResumeCaliente.push(new ResumeOrder(order.bocata.strIngredientes, 1, newBocataIngredientes));
        }
        else {

          var index = this.listResumeFrio.findIndex(x => x.ingredientes.sort().every(function (value, index) {
            return value === newBocataIngredientes.sort()[index]
          }));

          if (index !== -1)
            this.listResumeFrio[index].cantidad++;
          else
            this.listResumeFrio.push(new ResumeOrder(order.bocata.strIngredientes, 1, newBocataIngredientes));

        }

        this.listResumeCaliente = [...this.listResumeCaliente];
        this.listResumeFrio = [...this.listResumeFrio];

        totalPayOrders += Number(order.bocata.precio);
        this.totalBocata++;
      });

      this.totalPay = totalPayOrders.toFixed(2);

      if (this.listOrders && this.listOrders.length > 0) {
        this.getBookings();
        this.getVolunteer();
      }

      this.eventLoad.emit(false);
    });
  }

  getBookings() {
    if (this.listOrders && this.listOrders.length > 0) {
      this.listBookings = [];
      this.listOrders.forEach((ord) => {
        FirebaseDB.GetCollection('bookings/' + ord.user.uid + '/' + ord.bookingId, (snap: any) => {
          let booking = snap.val();
          this.listBookings.push(booking);
        });
      })
    }
  }

  getPayments() {
    FirebaseDB.GetCollection('payments/' + this.user.uid, (snap: any) => {
      this.listPayments = [];
      snap.forEach((request) => {
        request.forEach((request2) => {
          if (request2.val().typePay == 0 || request2.val().status == 1) {
            this.listPayments.push(new payment(
              request2.val().status,
              request2.val().bookingId,
              request2.val().date,
              request2.val().price,
              request2.val().photo,
              request2.val().name,
              request2.val().uid,
              request2.val().typePay
            ));
          }
        });
      });
    });
  }

  setAllowGPS() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        //if(!this.intervalAllowGPS){
        this.setVolunteerCoord(position); //now
        this.intervalAllowGPS = setInterval(() => {
          this.setVolunteerCoord(position)
        }, 180000)
        //}
      });
    }
  }

  //#endregion

  //#region Set
  setVolunteer() {
    let volunteer = new Volunteer(
      this.user.displayName,
      this.user.photoURL,
      Utils.GenerateDate('-'),
      this.user.uid,
      Utils.GenerateHour(),
      '',
      false
    );
    if (this.user && volunteer) {
      FirebaseDB.SetCollection('volunteer/' + Utils.GenerateDate('-'), volunteer,
        () => {
          this.volunteer = volunteer;

          if (volunteer.uid && this.listOrders && this.listOrders.length > 0)
            BR.Booking.SetVolunteer(this.volunteer, this.listOrders);

          //if (this.volunteer && this.volunteer.name)
          //  Utils.sendNotificationLasTime(volunteer.name);

          this.setAllowGPS();
        },
        (err) => { console.log(err) }
      );
    }
  }

  setOrder() {

    if (this.user &&
      this.volunteer &&
      this.listOrders &&
      this.listOrders.length > 0 &&
      this.listBookings &&
      this.listBookings.length > 0) {

      this.volunteer.finished = true;
      this.volunteer.HourEnd = Utils.GenerateHour();

      BR.Booking.SetStatus(this.volunteer, this.listBookings)
      BR.Booking.SetVolunteer(this.volunteer, this.listOrders);
      BR.Payments.Set(this.volunteer, this.listOrders);
      clearInterval(this.intervalAllowGPS);
    }
  }

  setVolunteerCoord(position: Position) {
    if (position) {
      let volunteerCoords = new VolunteerCoords();
      volunteerCoords.allowGPS = true;
      volunteerCoords.lastBeat = new Date();
      volunteerCoords.latitude = position.coords.latitude.toString();
      volunteerCoords.longitude = position.coords.longitude.toString();
      BR.Volunteers.SetVolunteerCoords(Utils.GenerateDate('-'), volunteerCoords);
    }
  }
  //#endregion

  //#region Handle

  //#region Handle
  handleCheckPay(payment: payment, typePay: number) {
    if (payment && this.volunteer && this.volunteer.uid) {
      payment.typePay = typePay;
      payment.status = 2;
      BR.Payments.SetByPayment(this.volunteer.uid, payment);
    }
  }
  //#endregion



  render() {
    return ([
      <stencil-route-title pageTitle="Voluntario"></stencil-route-title>,
      <div class='app-volunteer'>
        {this.listOrders.length == 0 ?
          <div class='a-v2'>
            No se han registrado pedidos para aún para presentarse voluntario
          </div>
          : (this.listOrders.length > 0) && !this.volunteer ?
            <div class='a-v'>
              <div class='a-v-t'>El voluntario</div>
              <div class='a-v-s'>Cualquier persona puede ofrecerse voluntario si alguien ha reservado.</div>

              <div class='app-volunteer-box'>

                <div class='a-v-s'>Haz click en este boton para informar al resto que te haces cargo de las reservas del día.</div>
                <div class='a-v-s'>Hoy tenemos <b>{this.listOrders.length} reserva/s pendiente/s</b>. Te animas? </div>
                <button class='a-v-b' onClick={() => this.setVolunteer()}>¡Me ofrezco hoy voluntario!</button>
              </div>
            </div>
            : this.listOrders.length > 0 &&
              this.volunteer &&
              this.volunteer.uid == this.user.uid &&
              !this.volunteer.finished ?
              <div class='a-v3'>
                <div class="a-v3-re">
                  <div class='a-v3-re-ti'>Reservas (agrupado)</div>
                  <div class='a-v3-re-sub'>Resumen de pedidos totales reservados</div>

                  <div class='app-volunteer-box'>


                    <div>Cantidad: {this.totalBocata.toString()}</div>
                    <div>Precio total: {this.totalPay}€</div>

                    {this.listResumeCaliente && this.listResumeCaliente.length > 0 ?
                      <div>
                        <div class='a-v3-re-su'>Calientes</div>
                        <div>
                          {this.listResumeCaliente.map((resume) =>
                            <div>{resume.cantidad}x {resume.bocata}</div>
                          )}
                        </div>
                      </div>
                      : null}

                    {this.listResumeFrio && this.listResumeFrio.length > 0 ?
                      <div>
                        <div class='a-v3-re-su'>Frios</div>
                        <div>
                          {this.listResumeFrio.map((resume) =>
                            <div>{resume.cantidad}x {resume.bocata}</div>
                          )}
                        </div>
                      </div>
                      : null}

                  </div>
                </div>
                <div class='a-v3-t'>Reservas (individual)</div>
                <p class='a-v3-tt'>Lo que cada usuario ha solicitado individualmente</p>
                <ul class='a-v3-u'>
                  {this.listOrders.map((order) =>
                    <li>
                      <span class='a-v3-u-t'>
                        <span class='a-v3-u-t-p'>
                          <img src={order.user ? order.user.photo : null}></img>
                        </span>
                        <span class='a-v3-u-t-n'>{order.user ? order.user.name : null}</span>
                        <span class='a-v3-u-t-i'>
                          <div>
                            {order.bocata ? order.bocata.strIngredientes : null}
                            {order.bocata.tostado ? <span class="a-v3-u-t-i-t">T</span> : null}
                          </div>
                        </span>
                        <span class='a-b-b-u-l-pay'>
                          <select class='app-booking-typepay' disabled>
                            <option value='0' selected={order.typePay == 0}>Forma de pago: no seleccionado</option>
                            <option value='1' selected={order.typePay == 1}>Bizum</option>
                            <option value='2' selected={order.typePay == 2}>Efectivo</option>
                          </select>
                        </span>
                        <span class='a-v3-u-t-r'>
                          {order.bocata ? order.bocata.precio : null}€
                      </span>
                      </span>
                    </li>
                  )}
                </ul>
                <div class='a-v3-x'><b>RECUERDA:</b> Márcalos como comprados una vez entregados y seguro que la entrega se ha efectuado correctamente.</div>
                <button class='a-v3-b' onClick={() => this.setOrder()}>Comprados</button>
              </div>
              : this.listOrders.length > 0 && this.volunteer ?
                <div class='a-v4'>
                  {!this.volunteer.finished ?
                    <div class='a-v4-an'>
                      <div class='a-v4-an-a'>El voluntario ha salido y pronto volverá con las reservas</div>
                      <div class='a-v4-an-a'><b>Hora de salida: {this.volunteer.HourStart}</b></div>
                      <div class='a-v4-an-b'>
                        <div class='a-v4-an-b-a'>
                          <img class='a-v4-an-b-a-a' src={this.volunteer.photo}></img>
                        </div>
                        <div class='a-v4-an-b-b'>
                          <app-svg name='shop' viewBox='0 0 489.4 489.4' width={50} height={50} color='#000'></app-svg>
                        </div>
                        <div class='cb'></div>
                      </div>
                      {<app-map></app-map>}
                    </div>

                    : this.volunteer.finished && this.volunteer && this.volunteer.uid == this.user.uid ?
                      [<span>
                        <img class='a-v4-p' src={this.volunteer.photo}></img>
                        <div class='a-v4-n'>{this.volunteer.name}</div>
                      </span>,
                      <div class='a-v4-s'>Gracias por ayudar a tusbocatas!</div>]
                      : this.volunteer.finished ?
                        [<span>
                          <img class='a-v4-p' src={this.volunteer.photo}></img>
                          <div class='a-v4-n'>{this.volunteer.name}</div>
                        </span>,
                        <div class='a-v4-s'>Ya puedes disfrutar de tu bocata! Dale las gracias a nuestro voluntario y no olvides pagarle! :)</div>]
                        : null}
                </div>
                : null}
        <div>
          {this.listPayments && this.listPayments.length > 0 ?
            <div class='a-v-pay'>
              <div class='a-v-pay-tit'>Pagos pendientes</div>
              <div class='a-v-pay-sub'>Este es el resumen de los pedidos pendientes de marcar como pagados</div>
              <ul class='app-volunteer-box a-v-pay-t'>
                {this.listPayments.map((payment) =>
                  <li class='a-v-pay-t-r'>
                    <span class='a-v-pay-r-f'>
                      <img src={payment.photo}></img>
                    </span>
                    <span>{payment.date}</span>
                    <span class='a-v-pay-r-n'>{payment.name}</span>
                    <span>{parseFloat(payment.price.toString()).toFixed(2)} €</span>
                    <span class='a-v-pay-t-r-t' onClick={() => this.handleCheckPay(payment, 1)}>Bizum</span>
                    <span class='a-v-pay-t-r-t' onClick={() => this.handleCheckPay(payment, 2)}>Efectivo</span>
                  </li>
                )}
              </ul>
            </div>
            : null}
        </div>
      </div>
    ]);
  }
}
