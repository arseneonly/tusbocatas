import { Component, State, Prop, Listen, EventEmitter, Event } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { FirebaseDB, BR } from '../../../helpers/firebaseDB';
import { Ingrediente, Bocata, Colors } from '../../../global/entities/bocata';
import { Utils } from '../../../global/utils';

@Component({
  tag: 'app-menu',
  styleUrl: 'app-menu.scss',
  shadow: true
})
export class AppMenu {

  @Prop() history: RouterHistory;

  @State() result: any;
  @State() listBocatasBestSeller: Array<Bocata> = [];
  @State() listBocatas: Array<Bocata> = [];
  @State() showNew: boolean = false;
  @State() user: any;

  @Event() eventLoad: EventEmitter;

  //#region Circle Life
  componentWillLoad() {
    FirebaseDB.AuthChange((user) => {

      if (user) {
        this.user = user;
        this.getRequest();
        Utils.checkNotification();
      }
      else
        this.history.push('/', {});
    })
  }

  //#endregion

  //#region Get
  getRequest() {

    this.listBocatas = [];
    this.listBocatasBestSeller = [];

    FirebaseDB.GetCollection('bestseller', (snap: any) => {
      this.listBocatasBestSeller = [];
      snap.forEach((request) => {
        let bocata = new Bocata();

        bocata.name = request.val().nombre;
        bocata.photo = request.val().photoUrl;
        bocata.precio = request.val().precio;
        bocata.strIngredientes = request.val().strIngredientes;
        bocata.tostado = (request.val().tostado ? true : false);

        if (request.val().colors && request.val().colors.color1 && request.val().colors.color2)
          bocata.colors = new Colors(request.val().colors.color1, request.val().colors.color2);

        bocata.ingredientes = [];

        for (var key in request.val().ingredientes) {
          bocata.ingredientes.push(new Ingrediente(
            request.val().ingredientes[key].name,
            request.val().ingredientes[key].icon,
            request.val().ingredientes[key].viewbox,
            request.val().ingredientes[key].width,
            request.val().ingredientes[key].height
          ));
        }

        this.listBocatasBestSeller = [...this.listBocatasBestSeller, bocata];
      });

      this.eventLoad.emit(false);
    });

    FirebaseDB.GetCollection('mybocata/' + this.user.uid, (snap: any) => {
      this.listBocatas = [];
      snap.forEach((request) => {
        let bocata = new Bocata();

        bocata.id = request.key;
        bocata.name = request.val().name;
        bocata.photo = request.val().photo;
        bocata.precio = request.val().precio;
        bocata.strIngredientes = request.val().strIngredientes;
        bocata.tostado = (request.val().tostado ? true : false);

        if (request.val().colors && request.val().colors.color1 && request.val().colors.color2)
          bocata.colors = new Colors(request.val().colors.color1, request.val().colors.color2);

        bocata.ingredientes = [];

        for (var key in request.val().ingredientes) {
          bocata.ingredientes.push(new Ingrediente(
            request.val().ingredientes[key].name,
            request.val().ingredientes[key].icon,
            request.val().ingredientes[key].viewbox,
            request.val().ingredientes[key].width,
            request.val().ingredientes[key].height
          ));
        }

        this.listBocatas = [...this.listBocatas, bocata];
      });
    });
  }
  //#endregion

  //#region Set
  setBooking(bocata: Bocata) {
    if (bocata) {
      BR.Booking.Add(this.user, bocata)
        .then(() => { this.history.push('/booking', {}); })
        .catch((err) => console.log(err))

      BR.Message.AddUser(this.user).catch((err) => console.log(err))
    }
  }

  setBocata(bocata: Bocata) {
    if (bocata) {
      BR.BLBocata.Remove(this.user.uid, bocata, () => {
        this.getRequest();
      });
    }
  }
  //#endregion

  //#region Handle
  handleNewBocata() {
    this.showNew = !this.showNew;
  }

  handleNewBooking(bocata: Bocata) {

    if (bocata && window.confirm("¿Estás seguro?")) {
      this.setBooking(bocata)
    }
  }

  handleRemove(bocata: Bocata) {
    if (bocata) {
      this.setBocata(bocata)
    }
  }
  //#endregion

  //#region Listen
  @Listen('handleCloseModal')
  handleCloseModal(event: CustomEvent) {
    this.showNew = event.detail;
    this.getRequest();
  }
  //#endregion



  render() {

    return ([
      <stencil-route-title pageTitle="Menu"></stencil-route-title>,
      <div class='app-menu'>
        <div class='app-menu-content'>
          {/*<div class='a-t'>
            {<div class='a-t-r' onClick={() => this.handleNewBocata()}>
              <div class='a-t-r-d'>Nuevo</div>
              <dic class='cb'></dic>
    </div>
            <div class='a-t-rr' onClick={() => this.handleNewBocata()}>
              <app-svg name="btn-plus" width={66} height={66} viewBox="0 0 57 57"></app-svg>
            </div>
            {this.showNew ?
              <div class='a-t-c'>
                {<app-new></app-new>}
              </div>
              : null}
            <div class='a-t-cb'></div>
            </div>*/}
          <section>
            <div class='app-menu-title'>Menú</div>
            <p class='app-menu-p'>Nuestra propuesta, una selección de nuestro bocadillos</p>
            <div class='app-menu-list'>
              {this.listBocatasBestSeller && this.listBocatasBestSeller.map((bocata) =>
                <div class="app-menu-list-item">
                  <div class="app-menu-list-item-price">
                    <span>{bocata.precio} €</span>
                  </div>
                  <div class='app-menu-list-item-tostado'>
                    {bocata.tostado ? <span title='Tostado' class='tostado'>T</span> : null}
                  </div>
                  <div class="app-menu-list-item-name">
                    {bocata.name}
                  </div>
                  <div class="app-menu-list-item-ing">
                    <div>{bocata.strIngredientes}</div>
                  </div>
                  <div class="app-menu-list-item-booking" onClick={() => this.handleNewBooking(bocata)}>
                    <div>Reservar</div>
                  </div>
                </div>
              )}
            </div>
          </section>
          <section>
            <div class='app-menu-title'>A mi gusto</div>
            <p class='app-menu-p'>Crea tu desayuno con tus ingredientes favoritos, tantos como quieras</p>
            <div class='app-menu-list'>
              {this.listBocatas && this.listBocatas.map((bocata) =>
                <div class="app-menu-list-item pers">
                  <div class="app-menu-list-item-price">
                    <span>{bocata.precio.toFixed(2)} €</span>
                  </div>
                  <div class='app-menu-list-item-tostado'>
                    {bocata.tostado ? <span title='Tostado' class='tostado'>T</span> : null}
                  </div>
                  <div class="app-menu-list-item-name">
                    {bocata.name}
                  </div>
                  <div class="app-menu-list-item-ing">
                    <div>{bocata.strIngredientes}</div>
                  </div>
                  <div class="app-menu-list-item-remove" onClick={() => this.handleRemove(bocata)}>
                    <div>Borrar</div>
                  </div>
                  <div class="app-menu-list-item-booking" onClick={() => this.handleNewBooking(bocata)}>
                    <div>Reservar</div>
                  </div>
                </div>
              )}

              <div class='app-menu-list-add'>
                <stencil-route-link url="/new" >
                  <span>+ Nuevo bocadillo</span>
                </stencil-route-link>
              </div>
            </div>
          </section>
        </div>
      </div>
    ]);
  }
}
