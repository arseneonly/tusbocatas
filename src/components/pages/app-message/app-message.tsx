import { Component, Prop, State, Event, EventEmitter } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { FirebaseDB, BR } from '../../../helpers/firebaseDB';
import { Message, MessageUser } from '../../../global/entities/message';
import { Utils } from '../../../global/utils';
import { Volunteer } from '../../../global/entities/volunteer';

@Component({
  tag: 'app-message',
  styleUrl: 'app-message.scss',
  shadow: true
})
export class AppMessage {

  @Prop() history: RouterHistory;

  @State() canRender: boolean = false;
  @State() photo: string = '';
  @State() name: string = '';
  @State() email: string = '';
  @State() uid: string = '';
  @State() canEntry: boolean = false;
  @State() text: string = '';
  @State() volunteer: Volunteer = null;

  @State() list: Array<Message> = [];
  @State() listUser: Array<MessageUser> = [];

  @Event() eventLoad: EventEmitter;

  //#region Circle Life
  componentWillLoad() {

    FirebaseDB.AuthChange((user) => {
      if (!user)
        this.history.push('/', {});
      else {
        this.canRender = true;
        this.name = user.displayName;
        this.email = user.email;
        this.photo = user.photoURL;
        this.uid = user.uid;
        this.eventLoad.emit(false);

        this.getMessagesUser();
      }
    });
  }
  //#endregion

  //#region Get
  getMessagesUser() {
    FirebaseDB.GetCollection('messages/' + Utils.GenerateDate('-') + '/participants/', (snap: any) => {
      this.listUser = [];
      if (snap) {
        snap.forEach((request) => {
          this.listUser.push(new MessageUser(
            request.val().photo,
            request.val().name,
            request.val().uid,
          ));
        });
      }
      if (this.listUser && this.listUser.length > 0) {
        let findMe = this.listUser.find(x => x.uid == this.uid);
        if (findMe || this.uid == 'wD9DfMdSSKPXvJaxL63dhLkIxqE2') {
          this.canEntry = true;
          this.getMessages();
        }
      }
    });
  }

  getMessages() {
    BR.Message.GetOrderBy(Utils.GenerateDate("-"), (snap: any) => {
      this.list = [];
      snap.forEach((request) => {
        this.list.push(new Message(
          request.val().photo,
          request.val().name,
          request.val().text,
          request.val().time,
          request.val().dateCreation
        ));
      });

      this.list = [...this.list];
      this.list.reverse();
    });
  }

  getVolunteer() {
    FirebaseDB.GetCollection('volunteer/' + Utils.GenerateDate('-'), (snap: any) => {
      if (snap && snap.val()) {
        this.volunteer = new Volunteer(
          snap.val().name,
          snap.val().photo,
          snap.val().date,
          snap.val().uid,
          snap.val().HourStart,
          snap.val().HourEnd,
          snap.val().finished
        );
      }
    });
  }
  //#endregion

  //#region Handle
  handleText(e: any) {
    this.text = e.target.value;
  }

  handleSave() {
    if (this.uid && this.photo && this.name && this.text) {
      BR.Message.AddText(new Message(this.photo, this.name, this.text, Utils.GenerateHour(), new Date))
        .then(() => { this.text = ''; })
        .catch((err) => console.log(err));
    }
  }

  //#endregion

  render() {
    return ([<stencil-route-title pageTitle="Mensajes"></stencil-route-title>,
    <div class="app-message">
      {!this.canEntry ?
        <div class='a-m-nd'>No dispones de ninguna reserva aún para entrar en al mensajería</div>
        : this.canEntry ?
          <div class='a-m'>
            <div class='a-m-tit'>
              Mensajería
              </div>
            <div class='a-m-i'>
              Grupo de comunicación entre personas que han efectuado una reserva hoy.
              </div>
            <div class='a-m-l'>
              {!this.list || this.list.length == 0 ?
                <div class='a-m-l-alert'>No se han encontrado mensajes</div>
                : this.list.map((message) =>
                  <div class='a-m-l-m'>
                    <span class='a-m-l-m-h'>{message.time}</span>
                    <img class='a-m-l-m-i' src={message.photo}></img>
                    <div class='a-m-l-m-r'>
                      <div class='a-m-l-m-r-n'>{message.name}</div>
                      <div class='a-m-l-m-r-t'>{message.text}</div>
                    </div>
                  </div>
                )}
            </div>
            <div class='a-m-t'>
              {this.list.length} comentarios
              </div>
            <div class='a-m-p'>
              {this.volunteer ? 
              <div>
                <span>Voluntario: <img src={this.volunteer.photo} title={this.volunteer.name}></img>{this.volunteer.name} </span>
              </div> 
              : null}
              <span>{this.listUser.length} participante/s:</span>
              {this.listUser.map((message) =>
                <img src={message.photo} title={message.name}></img>
              )}
            </div>
            <div class='a-m-m'>
              <textarea placeholder="Escribe un comentario para que lo vean el resto" onInput={(e) => this.handleText(e)} value={this.text}></textarea>
              <button class='a-p-d-g' onClick={() => this.handleSave()}>Enviar</button>
            </div>
          </div>
          : null}
    </div>
    ]);

  }
}
