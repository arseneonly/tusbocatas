import { Component, Prop, Element, Listen, State } from '@stencil/core';
import { RouterHistory, LocationSegments, injectHistory } from '@stencil/router';
import { FirebaseDB } from '../../helpers/firebaseDB';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.scss',
  shadow: true
})
export class AppRoot {

  @Element() el: HTMLStencilElement;
  @Prop() history: RouterHistory;
  @Prop() location: LocationSegments;


  @State() refreshUrlPhoto: string = '';
  @State() refreshPage: number = 0;
  @State() showLoading: boolean = true;
  @State() userLogged: boolean = false;

  //#region Circle Life

  componentWillLoad() {
    FirebaseDB.Init();
    this.getUser();
  }

  //#endregion

  //#region Listen
  @Listen('eventRefresh')
  listenRefreshPhoto(e: CustomEvent) {
    this.refreshUrlPhoto = e.detail;
  }

  @Listen('eventRefreshPage')
  listenRefreshPage(e: CustomEvent) {
    this.refreshPage = e.detail;
  }

  @Listen('eventLoad')
  listenLoad(e: CustomEvent) {
    this.showLoading = e.detail;
  }

  getUser = () => {
    FirebaseDB.AuthChange((user) => {
      if (user) 
        this.userLogged = user;
    });
  }


  //#endregion

  render() {
    return (
      <main>
        {this.showLoading ? <app-splash></app-splash> : null}
        <div class='app-me'>
          <div class="app-me-h">
            <app-header refreshUrlPhoto={this.refreshUrlPhoto} refreshPage={this.refreshPage}></app-header>
          </div>
          <div class={'app-me-b'+(!this.userLogged ? ' nologin' : '')}>
            <stencil-router titleSuffix=" - tusbocatas">
              <stencil-route-switch scrollTopOffset={0}>
                <stencil-route url='/' component='app-login' exact={true} />
                <stencil-route url='/profile' component='app-profile' />
                <stencil-route url='/menu' component='app-menu' />
                <stencil-route url='/booking' component='app-booking' />
                <stencil-route url='/volunteer' component='app-volunteer' />
                <stencil-route url='/message' component='app-message' />
                <stencil-route url='/admin' component='app-admin' />
                <stencil-route url='/new' component='app-new' />
                <stencil-route component="app-my" />
              </stencil-route-switch>
            </stencil-router>
          </div>
        </div>
      </main>
    );
  }
}

injectHistory(AppRoot);