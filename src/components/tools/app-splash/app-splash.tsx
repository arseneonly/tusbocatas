import { Component} from '@stencil/core';

@Component({
  tag: 'app-splash',
  styleUrl: 'app-splash.scss'
})
export class AppSplash {


  //#region Circle Life
  componentWillLoad() {

  }
  //#endregion



  render() {
    return ([
     
    <div class='app-splash'>
        <app-svg name="splash" width={300} height={300} viewBox="0 0 155 123" color="#E1524B"></app-svg>
    </div>
    ]);
  }
}
