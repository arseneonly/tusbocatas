import { Component, State } from '@stencil/core';
import { FirebaseDB } from '../../../helpers/firebaseDB';
import { Utils } from '../../../global/utils';
import { VolunteerCoords } from '../../../global/entities/volunteer';

@Component({
  tag: 'app-map',
  styleUrl: 'app-map.scss',
  shadow: true
})
export class AppMap {

  @State() volunteerCoords: VolunteerCoords = null;

  //#region Circle Life
  componentWillLoad() {
    this.getMap();
  }

  //#endregion

  //#region Get
  getMap() {
    FirebaseDB.GetCollection('volunteer/'+Utils.GenerateDate('-')+'/coords', (snap: any) => {
      if(snap && snap.val()){
        this.volunteerCoords = snap.val();
      }
    });
  }
  //#endregion

  render() {
    return ([
      <div class='app-map'>
        {this.volunteerCoords && this.volunteerCoords.allowGPS ?
          <div class='app-map-allow'>
            <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src={"https://www.openstreetmap.org/export/embed.html?bbox=2.284147739410401%2C41.59096335189514%2C2.287363708019257%2C41.592662438315955&layer=mapnik&marker=" + this.volunteerCoords.latitude +","+this.volunteerCoords.longitude} ></iframe>
            <div>Última sincronización: <b>{Utils.GenerateHourWithDate(':',this.volunteerCoords.lastBeat)}</b></div>
          </div>
          : <div class='app-map-noallow'>El voluntario no ha dado permiso para mostrar su ubicación</div>}
      </div>
    ]);
  }
}


