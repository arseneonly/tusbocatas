import { Component } from '@stencil/core';

@Component({
  tag: 'app-footer',
  styleUrl: 'app-footer.scss',
  shadow: true
})
export class AppHome {

  render() {
    return ([
      <div class='app-footer'>
        <p>footer</p>
      </div>
    ]);
  }
}
