# app-header



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description | Type     | Default |
| ----------------- | ------------------- | ----------- | -------- | ------- |
| `refreshPage`     | `refresh-page`      |             | `number` | `0`     |
| `refreshUrlPhoto` | `refresh-url-photo` |             | `string` | `''`    |


## Events

| Event              | Description | Type                |
| ------------------ | ----------- | ------------------- |
| `eventRefreshPage` |             | `CustomEvent<void>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
