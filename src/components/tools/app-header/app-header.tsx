import { Component, State, Prop, Event, EventEmitter } from '@stencil/core';
import { MenuHeader } from '../../../global/entities/menu';
import { FirebaseDB } from '../../../helpers/firebaseDB';
import { Utils } from '../../../global/utils';

@Component({
  tag: 'app-header',
  styleUrl: 'app-header.scss',
  shadow: true
})
export class AppHeader {

  @Prop() refreshUrlPhoto: string = '';
  @Prop() refreshPage: number = 0;

  @State() canRender: boolean = false;
  @State() name: string = '';
  @State() photo: string = '';
  @State() listMenu: Array<MenuHeader> = [];
  @State() showSubMenu: boolean = false;
  @State() showVolunteer: boolean = false;
  @State() open = false;


  @Event() eventRefreshPage: EventEmitter;

  //#region Circle Life
  componentWillLoad() {
    FirebaseDB.AuthChange((user) => {
      if (user) {
        this.getVolunteer();

        this.name = user.displayName;
        this.photo = user.photoURL;
        this.canRender = true;
        var idSelected = this.getPath();

        if (this.listMenu && this.listMenu.length == 0) {
          this.listMenu.push(new MenuHeader(1, 'Menú', '/menu', idSelected == 1, 'splash', '0 0 155 123'));
          this.listMenu.push(new MenuHeader(2, 'Reservas', '/booking', idSelected == 2, 'booking', '0 -36 512.001 512'));
          this.listMenu.push(new MenuHeader(4, 'Voluntario', '/volunteer', idSelected == 4, 'volunteer', '0 0 297 297'));
          this.listMenu.push(new MenuHeader(5, 'Mensajes', '/message', idSelected == 5, 'message', '0 0 512 512'));
          this.listMenu.push(new MenuHeader(6, 'Perfil', '/profile', idSelected == 6, 'profile', '0 0 512 512'));
        }
      }
    });
  }

  componentDidUpdate() {
    if (this.refreshUrlPhoto)
      this.photo = this.refreshUrlPhoto;
    if (this.listMenu && this.refreshPage > 0)
      this.handleSelectMenu(this.refreshPage);
  }
  //#endregion

  //#region Get
  getVolunteer() {
    FirebaseDB.GetCollection('volunteer/' + Utils.GenerateDate('-'), (snap: any) => {
      var currentUser = FirebaseDB.GetCurrentUser();
      if (snap && snap.val() && (currentUser && snap.val().uid != currentUser.uid) && !snap.val().finished) {
        this.showVolunteer = true;
        setTimeout(() => { this.showVolunteer = false }, 5000);
      }
    });
  }
  //#endregion

  //#region Handler

  handlerSignOut = () => {
    FirebaseDB.SignOut(
      () => { window.location.href = "/"; },
      () => { }
    )
  }

  handleSelectMenu(id: number) {
    if (this.refreshPage > 0)
      this.eventRefreshPage.emit(0);

    if (this.listMenu && this.listMenu.length > 0) {
      this.listMenu.forEach((item) => { item.selected = false; });
      this.listMenu.find(x => x.id == id).selected = true;
      this.listMenu = [...this.listMenu];
    }
  }

  handeSelectSubMenu() {
    this.showSubMenu = !this.showSubMenu;
  }

  //#endregion

  //#region Utils
  getPath() {
    let id = 1;

    if (window.location.pathname == '/menu')
      id = 1;
    else if (window.location.pathname == '/booking')
      id = 2;
    else if (window.location.pathname == '/volunteer')
      id = 4;
    else if (window.location.pathname == '/message')
      id = 5;
    else if (window.location.pathname == '/profile')
      id = 6;

    return id;
  }
  //#endregion

  render() {
    return (
      this.canRender ?
        <div class='app-header '>
          {/*{this.showVolunteer && this.canRender ?
          <div class='a-h-v'>
            El voluntario esta tramitando las
            <stencil-route-link url='/volunteer'>
              reservas
            </stencil-route-link>
          </div>
        : null}*/}
          <div>
            <div class={`app-header-horizontal-overlay ${this.open ? 'open' : ''}`} onClick={() => { this.open = !this.open }}></div>
            <div class='app-header-horizontal'>
              <h1>tusbocatas</h1>
              <app-svg name="menu" viewBox="0 0 30 30" width={30} height={30} color="#000" onClick={() => { this.open = !this.open }}></app-svg>
            </div>
            <div class={`app-header-vertical ${this.open ? 'open' : ''}`}>
              <div class='app-header-logo'>
                <app-svg name="splash-white" width={100} height={100} viewBox="0 0 155 123" color="#000" onClick={() => { this.open = !this.open }}></app-svg>
                <h1>
                  tusbocatas
                </h1>
              </div>
              <div class='app-header-menu'>
                {this.listMenu && this.listMenu.map((todo) =>
                  <li class={todo && todo.selected ? 'active' : null}>
                    <stencil-route-link url={todo.url} onClick={() => this.handleSelectMenu(todo.id)}>
                      <span>{todo.text}</span>
                    </stencil-route-link>
                  </li>
                )}
              </div>
              <div class='app-header-user'>
                <img class='app-header-user-img' src={this.photo}></img>
                <div class='app-header-user-name'>{this.name}</div>
                <div class='app-header-user-close' onClick={() => this.handlerSignOut()}>Cerrar sesión</div>
              </div>
            </div>
          </div>
        </div>
        : null
    );
  }
}
