# app-svg



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description | Type     | Default |
| --------- | ---------- | ----------- | -------- | ------- |
| `class`   | `class`    |             | `string` | `""`    |
| `color`   | `color`    |             | `string` | `""`    |
| `height`  | `height`   |             | `number` | `0`     |
| `name`    | `name`     |             | `string` | `""`    |
| `viewBox` | `view-box` |             | `string` | `""`    |
| `width`   | `width`    |             | `number` | `0`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
